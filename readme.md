## Gobii Keycloak

Helper startup scripts and Themes.

## Themes
The themes directory contains the themes that will be deployed to the Keycloak instance.
Documentation can be found on [Keycloak's](https://www.keycloak.org/docs/latest/server_development/index.html#_themes) offical documentation.

#### Theme Development 
To develop the themes you will need a local keycloak instance to install the theme for previewing.

1. From the root of this repo
1. Start Keycloak instance: `docker-compose up -d`
1. Add a user in keycloak: `docker exec keycloak keycloak/bin/add-user-keycloak.sh -u admin -p admin`
1. Login to http://localhost:8080/auth/admin/
1. Click 'Themes' tab
1. Change the relevant theme to 'gobii' & save
1. Logout
1. Open http://localhost:8080/auth/realms/master/account to see the login page.
1. Changes in this repo should relect on refreshing your browser.

## Scripts
Contains bash scripts that are used to assist deployments. They are more useful mounted into the keycloak container than here, for eaxmple the themeloader will clone this repo and copy the themes to the required directories.