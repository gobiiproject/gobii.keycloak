#!/usr/bin/env bash

    if [ "$#" -ne 2 ]; then
      echo "Incorrect usage"
      echo "Usage:"
      echo "   keycloakthemeloader <bitbucket_repo> <bitbucket_branch>"
      exit 1
    fi

    bitbucket_repo=$1
    bitbucket_branch=$2

    # Get the OAuth bearer token
    result=$(curl -X POST -u $BITBUCKET_KEY:$BITBUCKET_SECRET --data "grant_type=client_credentials" https://bitbucket.org/site/oauth2/access_token )

    # Extract the token from the result
    token=$(echo $result | jq -r .access_token)

    # Download the repo
    curl -H "Authorization: Bearer $token" https://${BITBUCKET_USER}@bitbucket.org/${bitbucket_repo}/get/${bitbucket_branch}.zip -o out.zip

    # Unzip just the themes directory
    unzip -u out.zip -d ./out

    # copy the themes directory to Keycloak themes directory
    cp -r ./out/*/themes/* /opt/jboss/keycloak/themes

    # tidy up
    rm -rf ./out
    rm -f out.zip

    set -eu
    
  