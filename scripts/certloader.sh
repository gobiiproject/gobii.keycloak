#!/usr/bin/env bash

    # Load the cert into the truststore
    if [ $USE_TRUSTSTORE ]; then
      echo "$IMPORT_CERT" | sed 's/\\n/\n/g' >  ca.crt
      $JAVA_HOME/bin/keytool -import -alias HOSTDOMAIN -keystore /opt/jboss/certs.jks -file /opt/jboss/ca.crt -storepass ${STORE_PASS:-password} -noprompt -trustcacerts
    fi